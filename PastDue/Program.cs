﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace PastDue
{
    class Program
    {
        static void Main(string[] args)
        {

            string path = "server=198.143.98.122;database=veritas;User Id=sa;Password=NCC1701E";
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            
            insertPastDue();

            calculateCurrentDealer();
            calculateLastWeekDealer();
            calculateDifferenceForDealer();

            calculateCurrentAgent();
            calculateLastWeekAgent();
            calculateDifferenceForAgent();



            void insertPastDue()
            {
                string sSQL = "select * from veritasreports.dbo.pastduedata";
                dBO.dboOpen(path);
                DataTable dt = dBO.dboInportInformation(sSQL);
                
                if (dt.Rows.Count == 0)
                {
                    sSQL = "insert into veritasreports.dbo.pastduedata " +
                              "select CONVERT(DATE, getdate()) as WeekDate, contractid, dealerid, AgentsID, DATEDIFF(d, SaleDate, getdate()) as daysoverdue from veritas.dbo.contract " +
                              "where status = 'Pending' " +
                              "and not fname like '%demo%' " +
                              "and not fname like '%test%' " +
                              "and not contractno like 'rac%' " +
                              "and not contractno like 'rep%' " +
                              "and not contractno like 'vep%' " +
                              "and not programid in (47, 48, 51, 54, 55, 56, 58, 59, 60, 64, 67, 103, 105) " +
                              "and DATEDIFF(d, SaleDate, getdate()) > 29 " +
                              "and dealerid <> 5177 ";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }

                sSQL = "delete from veritasreports.dbo.pastduedata where weekdate <= '" + DateTime.Now.AddDays(-12).ToString("MM-dd-yyyy") + "' " +
                    "truncate table VeritasPowerBI.dbo.DealerPastDue " +
                    "truncate table VeritasPowerBI.dbo.AgentPastDue ";
                dBO.dboAlterDBOUnsafe(sSQL);

                dt = dBO.dboInportInformation("select * from veritasreports.dbo.pastduedata");

                if (dt.Rows[0]["weekdate"].ToString() == dt.Rows[dt.Rows.Count-1]["weekdate"].ToString())
                {
                    sSQL = "insert into veritasreports.dbo.pastduedata " +
                        "select CONVERT(DATE, getdate()) as WeekDate, contractid, dealerid, AgentsID, DATEDIFF(d, SaleDate, getdate()) as daysoverdue from veritas.dbo.contract " +
                        "where status = 'Pending' " +
                        "and not fname like '%demo%' " +
                        "and not fname like '%test%' " +
                        "and not contractno like 'rac%' " +
                        "and not contractno like 'rep%' " +
                        "and not contractno like 'vep%' " +
                        "and not programid in (47, 48, 51, 54, 55, 56, 58, 59, 60, 64, 67, 103, 105) " +
                        "and DATEDIFF(d, SaleDate, getdate()) > 29 " +
                        "and dealerid <> 5177 ";
                      dBO.dboAlterDBOUnsafe(sSQL);
                }
                dBO.dboClose();
            }

            void calculateCurrentDealer()
            {
                
                string sSQL = "select top 10 DealerID, count(DaysOverDue) as Total from VeritasReports.dbo.PastDueData " +
                                "where WeekDate >= '" + DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + "' " +
                                "group by DealerID " +
                                "order by count(DaysOverDue) desc ";

                string allDataSQL = "select * from VeritasReports.dbo.PastDueData pd " +
                                 "inner join (select top 10 DealerID, count(DaysOverDue) as Total from VeritasReports.dbo.PastDueData " +
                                 "where WeekDate >= '" + DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") +"' "+
                                 "group by DealerID " +
                                 "order by count(DaysOverDue) desc) c on pd.DealerID = c.DealerID " +
                                 "where WeekDate >= '" + DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + "' "+
                                 "order by Total desc ";
                
                dBO.dboOpen(path);
                DataTable top10Dealers = dBO.dboInportInformation(sSQL);
                DataTable allPastDue = dBO.dboInportInformation(allDataSQL);
                dBO.dboClose();

                int dealerid;
                string type;
                int total;
                int days3059=0;
                int days3044=0;
                int days4559=0;
                int days6089=0;
                int days90119=0;
                int daysover120=0;

                DataTable calculatedTopTen = new DataTable("DealerPastDue");
                DataColumn[] cols = { new DataColumn("DealerID", typeof(Int32)),
                                  new DataColumn("Type", typeof(String)),
                                  new DataColumn("Total", typeof(Int32)),
                                  new DataColumn("Days3059", typeof(Int32)),
                                  new DataColumn("Days3044", typeof(Int32)),
                                  new DataColumn("Days4559", typeof(Int32)),
                                  new DataColumn("Days6089", typeof(Int32)),
                                  new DataColumn("Days90119", typeof(Int32)),
                                  new DataColumn("DaysOver120", typeof(Int32))
                              };
                calculatedTopTen.Columns.AddRange(cols);

                foreach (DataRow dr in top10Dealers.Rows)
                {
                    dealerid = (int)dr["dealerid"];
                    type = "Current";
                    total = (int)dr["total"];
                    foreach (DataRow drr in allPastDue.Rows)
                    {
                        if (dealerid == (int)drr["dealerid"])
                        {
                            if ((int)drr["daysoverdue"] > 29 && (int)drr["daysoverdue"] < 60)
                            {
                                days3059 = days3059 + 1;
                                if ((int)drr["daysoverdue"] > 29 && (int)drr["daysoverdue"] < 45)
                                {
                                    days3044 = days3044 + 1;
                                }
                                else if ((int)drr["daysoverdue"] > 44 && (int)drr["daysoverdue"] < 60)
                                {
                                    days4559 = days4559 + 1;
                                }
                            }
                            else if ((int)drr["daysoverdue"] > 59 && (int)drr["daysoverdue"] < 90)
                            {
                                days6089 = days6089 + 1;
                            }
                            else if ((int)drr["daysoverdue"] > 89 && (int)drr["daysoverdue"] < 120)
                            {
                                days90119 = days90119 + 1;
                            }
                            else if ((int)drr["daysoverdue"] > 120)
                            {
                                daysover120 = daysover120 + 1;
                            }
                        }
                    }
                    DataRow newRow = calculatedTopTen.NewRow();
                    newRow[0] = dealerid;
                    newRow[1] = type;
                    newRow[2] = total;
                    newRow[3] = days3059;
                    newRow[4] = days3044;
                    newRow[5] = days4559;
                    newRow[6] = days6089;
                    newRow[7] = days90119;
                    newRow[8] = daysover120;
                    calculatedTopTen.Rows.Add(newRow);
                    days3059 = 0;
                    days3044 = 0;
                    days4559 = 0;
                    days6089 = 0;
                    days90119 = 0;
                    daysover120 = 0;
                }
                using (SqlConnection connection = new SqlConnection(path))
                {
                    connection.Open();
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                    {
                        foreach (DataColumn c in calculatedTopTen.Columns)
                        {
                            bulkCopy.ColumnMappings.Add(c.ColumnName, c.ColumnName);
                        }
                        bulkCopy.DestinationTableName = "veritaspowerbi.dbo." + calculatedTopTen.TableName;
                        try
                        {
                            bulkCopy.WriteToServer(calculatedTopTen);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                    connection.Close();
                }
            }

            void calculateLastWeekDealer()
            {
                string dealersSQL = "select * from veritaspowerbi.dbo.dealerpastdue " +
                                        "where type = 'Current' ";
                dBO.dboOpen(path);
                DataTable currentDealers = dBO.dboInportInformation(dealersSQL);
                

                foreach (DataRow cdr in currentDealers.Rows)
                {

                    string sSQL = "select DealerID, count(DaysOverDue) as Total from VeritasReports.dbo.PastDueData " +
                                    "where weekdate <= '" + DateTime.Now.AddDays(-6).ToString("MM/dd/yyyy") + "' and " +
                                    "DealerID = " + (int)cdr["dealerid"] + " " +
                                    "group by DealerID " +
                                    "order by count(DaysOverDue) desc ";

                    string allDataSQL = "select * from VeritasReports.dbo.PastDueData pd " +
                                              "inner join (select DealerID, count(DaysOverDue) as Total from VeritasReports.dbo.PastDueData " +
                                              "where weekdate <= '" + DateTime.Now.AddDays(-6).ToString("MM/dd/yyyy") + "' and " +
                                              "DealerID = " + (int)cdr["dealerid"] + " " +
                                              "group by DealerID) c on pd.DealerID = c.DealerID " +
                                              "where WeekDate <= '" + DateTime.Now.AddDays(-6).ToString("MM/dd/yyyy") + "' " +
                                              "order by Total desc ";
                        

                    DataTable top10Dealers = dBO.dboInportInformation(sSQL);
                    DataTable allPastDue = dBO.dboInportInformation(allDataSQL);
                    
                    int dealerid;
                    string type;
                    int total;
                    int days3059 = 0;
                    int days3044 = 0;
                    int days4559 = 0;
                    int days6089 = 0;
                    int days90119 = 0;
                    int daysover120 = 0;

                    DataTable calculatedTopTen = new DataTable("DealerPastDue");
                    DataColumn[] cols = { new DataColumn("DealerID", typeof(Int32)),
                                      new DataColumn("Type", typeof(String)),
                                      new DataColumn("Total", typeof(Int32)),
                                      new DataColumn("Days3059", typeof(Int32)),
                                      new DataColumn("Days3044", typeof(Int32)),
                                      new DataColumn("Days4559", typeof(Int32)),
                                      new DataColumn("Days6089", typeof(Int32)),
                                      new DataColumn("Days90119", typeof(Int32)),
                                      new DataColumn("DaysOver120", typeof(Int32))
                                  };
                    calculatedTopTen.Columns.AddRange(cols);
                    
                    foreach (DataRow dr in top10Dealers.Rows)
                    {
                        dealerid = (int)dr["dealerid"];
                        type = "Previous";
                        total = (int)dr["total"];
                        foreach (DataRow drr in allPastDue.Rows)
                        {
                            if (dealerid == (int)drr["dealerid"])
                            {
                                if ((int)drr["daysoverdue"] > 29 && (int)drr["daysoverdue"] < 60)
                                {
                                    days3059 = days3059 + 1;
                                    if ((int)drr["daysoverdue"] > 29 && (int)drr["daysoverdue"] < 45)
                                    {
                                        days3044 = days3044 + 1;
                                    }
                                    else if ((int)drr["daysoverdue"] > 44 && (int)drr["daysoverdue"] < 60)
                                    {
                                        days4559 = days4559 + 1;
                                    }
                                }
                                else if ((int)drr["daysoverdue"] > 59 && (int)drr["daysoverdue"] < 90)
                                {
                                    days6089 = days6089 + 1;
                                }
                                else if ((int)drr["daysoverdue"] > 89 && (int)drr["daysoverdue"] < 120)
                                {
                                    days90119 = days90119 + 1;
                                }
                                else if ((int)drr["daysoverdue"] > 120)
                                {
                                    daysover120 = daysover120 + 1;
                                }
                            }
                        }
                        DataRow newRow = calculatedTopTen.NewRow();
                        newRow[0] = dealerid;
                        newRow[1] = type;
                        newRow[2] = total;
                        newRow[3] = days3059;
                        newRow[4] = days3044;
                        newRow[5] = days4559;
                        newRow[6] = days6089;
                        newRow[7] = days90119;
                        newRow[8] = daysover120;
                        calculatedTopTen.Rows.Add(newRow);
                        days3059 = 0;
                        days3044 = 0;
                        days4559 = 0;
                        days6089 = 0;
                        days90119 = 0;
                        daysover120 = 0;
                    }

                    using (SqlConnection connection = new SqlConnection(path))
                    {
                        connection.Open();
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                        {
                            foreach (DataColumn c in calculatedTopTen.Columns)
                            {
                                bulkCopy.ColumnMappings.Add(c.ColumnName, c.ColumnName);
                            }
                            bulkCopy.DestinationTableName = "veritaspowerbi.dbo." + calculatedTopTen.TableName;
                            try
                            {
                                bulkCopy.WriteToServer(calculatedTopTen);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.ToString());
                            }
                        }
                        connection.Close();
                    }
                }
                dBO.dboClose();
            }

            void calculateDifferenceForDealer()
            {

                string sSQL = "SELECT pd.DealerID, 'Difference' AS Type, pd.Total - apd.Total AS 'Total', " +
                                "pd.Days3059 - apd.Days3059 as '30-59 Days', " +
                                "pd.Days3044 - apd.Days3044 as '30-44 Days', " +
                                "pd.Days4559 - apd.Days4559 as '45-59 Days', " +
                                "pd.Days6089 - apd.Days6089 as '60-89 Days', " +
                                "pd.Days90119 - apd.Days90119 as '90-119 Days', " +
                                "pd.DaysOver120 - apd.DaysOver120 as 'Over 120 Days' " +
                                "FROM VeritasPowerBI.dbo.DealerPastDue pd " +
                                "LEFT OUTER JOIN(select * from VeritasPowerBI.dbo.DealerPastDue where Type = 'Previous') apd " +
                                "ON apd.DealerID = pd.DealerID " +
                                "where pd.Type = 'Current'";

                dBO.dboOpen(path);
                DataTable diffTable = dBO.dboInportInformation(sSQL);
                
                foreach (DataRow dr in diffTable.Rows)
                {
                    sSQL = "insert into VeritasPowerBI.dbo.DealerPastDue " +
                            "Values('" + dr["dealerid"] + "', '" + dr["type"] + "', '" + dr["total"] + "', '" +
                            dr["30-59 Days"] + "', '" + dr["30-44 Days"] + "', '" + dr["45-59 Days"] + "', '" + dr["60-89 Days"] + "', '" +
                            dr["90-119 Days"] + "', '" + dr["Over 120 Days"] + "')";
                    
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
                dBO.dboClose();
            }

            void calculateCurrentAgent()
            {
                string sSQL = "select top 10 AgentsID, count(DaysOverDue) as Total from VeritasReports.dbo.PastDueData " +
                                "where weekdate >= '" + DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + "' " +
                                "group by AgentsID " +
                                "order by count(DaysOverDue) desc ";

                string allDataSQL = "select * from VeritasReports.dbo.PastDueData pd " +
                                 "inner join (select top 10 AgentsID, count(DaysOverDue) as Total from VeritasReports.dbo.PastDueData " +
                                 "where weekdate >= '" + DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + "' " +
                                 "group by AgentsID " +
                                 "order by count(DaysOverDue) desc) c on pd.AgentsID = c.AgentsID " +
                                 "where WeekDate >= '" + DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy") + "' " +
                                 "order by Total desc ";

                dBO.dboOpen(path);
                DataTable top10Agents = dBO.dboInportInformation(sSQL);
                DataTable allPastDue = dBO.dboInportInformation(allDataSQL);
                dBO.dboClose();

                int agentsid;
                string type;
                int total;
                int days3059 = 0;
                int days3044 = 0;
                int days4559 = 0;
                int days6089 = 0;
                int days90119 = 0;
                int daysover120 = 0;

                DataTable calculatedTopTen = new DataTable("AgentPastDue");
                DataColumn[] cols = { new DataColumn("AgentID", typeof(Int32)),
                                  new DataColumn("Type", typeof(String)),
                                  new DataColumn("Total", typeof(Int32)),
                                  new DataColumn("Days3059", typeof(Int32)),
                                  new DataColumn("Days3044", typeof(Int32)),
                                  new DataColumn("Days4559", typeof(Int32)),
                                  new DataColumn("Days6089", typeof(Int32)),
                                  new DataColumn("Days90119", typeof(Int32)),
                                  new DataColumn("DaysOver120", typeof(Int32))
                              };
                calculatedTopTen.Columns.AddRange(cols);

                foreach (DataRow dr in top10Agents.Rows)
                {
                    agentsid = (int)dr["agentsid"];
                    type = "Current";
                    total = (int)dr["total"];
                    foreach (DataRow drr in allPastDue.Rows)
                    {
                        if (agentsid == (int)drr["agentsid"])
                        {
                            if ((int)drr["daysoverdue"] > 29 && (int)drr["daysoverdue"] < 60)
                            {
                                days3059 = days3059 + 1;
                                if ((int)drr["daysoverdue"] > 29 && (int)drr["daysoverdue"] < 45)
                                {
                                    days3044 = days3044 + 1;
                                }
                                else if ((int)drr["daysoverdue"] > 44 && (int)drr["daysoverdue"] < 60)
                                {
                                    days4559 = days4559 + 1;
                                }
                            }
                            else if ((int)drr["daysoverdue"] > 59 && (int)drr["daysoverdue"] < 90)
                            {
                                days6089 = days6089 + 1;
                            }
                            else if ((int)drr["daysoverdue"] > 89 && (int)drr["daysoverdue"] < 120)
                            {
                                days90119 = days90119 + 1;
                            }
                            else if ((int)drr["daysoverdue"] > 120)
                            {
                                daysover120 = daysover120 + 1;
                            }
                        }
                    }
                    DataRow newRow = calculatedTopTen.NewRow();
                    newRow[0] = agentsid;
                    newRow[1] = type;
                    newRow[2] = total;
                    newRow[3] = days3059;
                    newRow[4] = days3044;
                    newRow[5] = days4559;
                    newRow[6] = days6089;
                    newRow[7] = days90119;
                    newRow[8] = daysover120;
                    calculatedTopTen.Rows.Add(newRow);
                    days3059 = 0;
                    days3044 = 0;
                    days4559 = 0;
                    days6089 = 0;
                    days90119 = 0;
                    daysover120 = 0;
                }
                using (SqlConnection connection = new SqlConnection(path))
                {
                    connection.Open();
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                    {
                        foreach (DataColumn c in calculatedTopTen.Columns)
                        {
                            bulkCopy.ColumnMappings.Add(c.ColumnName, c.ColumnName);
                        }
                        bulkCopy.DestinationTableName = "veritaspowerbi.dbo." + calculatedTopTen.TableName;
                        try
                        {
                            bulkCopy.WriteToServer(calculatedTopTen);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                    connection.Close();
                }
            }

            void calculateLastWeekAgent()
            {
                string dealersSQL = "select * from veritaspowerbi.dbo.agentpastdue " +
                                        "where type = 'Current' ";
                dBO.dboOpen(path);
                DataTable currentDealers = dBO.dboInportInformation(dealersSQL);
                

                foreach (DataRow cdr in currentDealers.Rows)
                {

                    string sSQL = "select AgentsID, count(DaysOverDue) as Total from VeritasReports.dbo.PastDueData " +
                                    "where weekdate <= '" + DateTime.Now.AddDays(-6).ToString("MM/dd/yyyy") + "' and " +
                                    "AgentsID = " + (int)cdr["agentid"] + " " +
                                    "group by agentsID " +
                                    "order by count(DaysOverDue) desc ";

                    string allDataSQL = "select * from VeritasReports.dbo.PastDueData pd " +
                                              "inner join (select AgentsID, count(DaysOverDue) as Total from VeritasReports.dbo.PastDueData " +
                                              "where weekdate <= '" + DateTime.Now.AddDays(-6).ToString("MM/dd/yyyy") + "' and " +
                                              "AgentsID = " + (int)cdr["agentid"] + " " +
                                              "group by AgentsID) c on pd.agentsID = c.agentsID " +
                                              "where WeekDate <= '" + DateTime.Now.AddDays(-6).ToString("MM/dd/yyyy") + "' " +
                                              "order by Total desc ";


                    DataTable top10Agents = dBO.dboInportInformation(sSQL);
                    DataTable allPastDue = dBO.dboInportInformation(allDataSQL);
                    
                    int agentsid;
                    string type;
                    int total;
                    int days3059 = 0;
                    int days3044 = 0;
                    int days4559 = 0;
                    int days6089 = 0;
                    int days90119 = 0;
                    int daysover120 = 0;

                    DataTable calculatedTopTen = new DataTable("AgentPastDue");
                    DataColumn[] cols = { new DataColumn("AgentID", typeof(Int32)),
                                      new DataColumn("Type", typeof(String)),
                                      new DataColumn("Total", typeof(Int32)),
                                      new DataColumn("Days3059", typeof(Int32)),
                                      new DataColumn("Days3044", typeof(Int32)),
                                      new DataColumn("Days4559", typeof(Int32)),
                                      new DataColumn("Days6089", typeof(Int32)),
                                      new DataColumn("Days90119", typeof(Int32)),
                                      new DataColumn("DaysOver120", typeof(Int32))
                                  };
                    calculatedTopTen.Columns.AddRange(cols);

                    foreach (DataRow dr in top10Agents.Rows)
                    {
                        agentsid = (int)dr["agentsid"];
                        type = "Previous";
                        total = (int)dr["total"];
                        foreach (DataRow drr in allPastDue.Rows)
                        {
                            if (agentsid == (int)drr["agentsid"])
                            {
                                if ((int)drr["daysoverdue"] > 29 && (int)drr["daysoverdue"] < 60)
                                {
                                    days3059 = days3059 + 1;
                                    if ((int)drr["daysoverdue"] > 29 && (int)drr["daysoverdue"] < 45)
                                    {
                                        days3044 = days3044 + 1;
                                    }
                                    else if ((int)drr["daysoverdue"] > 44 && (int)drr["daysoverdue"] < 60)
                                    {
                                        days4559 = days4559 + 1;
                                    }
                                }
                                else if ((int)drr["daysoverdue"] > 59 && (int)drr["daysoverdue"] < 90)
                                {
                                    days6089 = days6089 + 1;
                                }
                                else if ((int)drr["daysoverdue"] > 89 && (int)drr["daysoverdue"] < 120)
                                {
                                    days90119 = days90119 + 1;
                                }
                                else if ((int)drr["daysoverdue"] > 120)
                                {
                                    daysover120 = daysover120 + 1;
                                }
                            }
                        }
                        DataRow newRow = calculatedTopTen.NewRow();
                        newRow[0] = agentsid;
                        newRow[1] = type;
                        newRow[2] = total;
                        newRow[3] = days3059;
                        newRow[4] = days3044;
                        newRow[5] = days4559;
                        newRow[6] = days6089;
                        newRow[7] = days90119;
                        newRow[8] = daysover120;
                        calculatedTopTen.Rows.Add(newRow);
                        days3059 = 0;
                        days3044 = 0;
                        days4559 = 0;
                        days6089 = 0;
                        days90119 = 0;
                        daysover120 = 0;
                    }

                    using (SqlConnection connection = new SqlConnection(path))
                    {
                        connection.Open();
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                        {
                            foreach (DataColumn c in calculatedTopTen.Columns)
                            {
                                bulkCopy.ColumnMappings.Add(c.ColumnName, c.ColumnName);
                            }
                            bulkCopy.DestinationTableName = "veritaspowerbi.dbo." + calculatedTopTen.TableName;
                            try
                            {
                                bulkCopy.WriteToServer(calculatedTopTen);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.ToString());
                            }
                        }
                        connection.Close();
                    }
                }
                dBO.dboClose();
            }

            void calculateDifferenceForAgent()
            {
                string sSQL = "SELECT pd.AgentID, 'Difference' AS Type, pd.Total - apd.Total AS 'Total', " +
                                "pd.Days3059 - apd.Days3059 as '30-59 Days', " +
                                "pd.Days3044 - apd.Days3044 as '30-44 Days', " +
                                "pd.Days4559 - apd.Days4559 as '45-59 Days', " +
                                "pd.Days6089 - apd.Days6089 as '60-89 Days', " +
                                "pd.Days90119 - apd.Days90119 as '90-119 Days', " +
                                "pd.DaysOver120 - apd.DaysOver120 as 'Over 120 Days' " +
                                "FROM VeritasPowerBI.dbo.AgentPastDue pd " +
                                "LEFT OUTER JOIN(select * from VeritasPowerBI.dbo.AgentPastDue where Type = 'Previous') apd " +
                                "ON apd.AgentID = pd.AgentID " +
                                "where pd.Type = 'Current'";

                dBO.dboOpen(path);
                DataTable diffTable = dBO.dboInportInformation(sSQL);
                
                foreach (DataRow dr in diffTable.Rows)
                {
                    sSQL = "insert into VeritasPowerBI.dbo.AgentPastDue " +
                            "Values('" + dr["agentid"] + "', '" + dr["type"] + "', '" + dr["total"] + "', '" + 
                            dr["30-59 Days"] + "', '" + dr["30-44 Days"] + "', '" + dr["45-59 Days"] + "', '" + dr["60-89 Days"] + "', '" +
                            dr["90-119 Days"] + "', '" + dr["Over 120 Days"] + "')";
                    
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
                dBO.dboClose();
            }

        }
    }
}
